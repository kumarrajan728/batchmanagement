﻿using BatchManagement.API.DataAccessLayer;
using BatchManagement.API.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

// Maine change kiya hai - Pawan
namespace BatchManagement.API.Controllers
{
    [RoutePrefix("api/student")]
    public class StudentController : ApiController
    {
        [HttpGet]
        [Route("getall")]
        public List<Student> GetAllStudents()
        {
            using (BatchContext context = new BatchContext())
            {
                List<Student> students = context.students.ToList();
                return students;
            }
        }

        [HttpGet]
        [Route("get")]
        public Student GetStudent(int id)
        {
            using (BatchContext context = new BatchContext())
            {
                Student student = context.students.Find(id);
                return student;
            }
        }

        [HttpPost]
        [Route("new")]
        public void CreateStudent(string name, int batchId)
        {
            using (BatchContext context = new BatchContext())
            {
                Student student = new Student() { Name = name, BatchId = batchId };

                context.students.Add(student);

                context.SaveChanges();
            }
        }

        [HttpPut]
        [Route("update")]
        public void UpdateStudent(int id, string name, int batchId)
        {
            using (BatchContext context = new BatchContext())
            {
                Student student = context.students.Find(id);
                student.Name = name;
                student.BatchId = batchId;

                context.SaveChanges();
            }
        }

        [HttpDelete]
        [Route("delete")]
        public void DeleteStudent(int id)
        {
            using (BatchContext context = new BatchContext())
            {
                Student student = context.students.Find(id);
                context.students.Remove(student);

                context.SaveChanges();
            }
        }
    }
}
