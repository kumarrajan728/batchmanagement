﻿using System.Collections.Generic;
using System.Web.Http;
using BatchManagement.API.DataAccessLayer;
using BatchManagement.API.Models;
using System.Linq;

namespace BatchManagement.API.Controllers
{
    [RoutePrefix("api/batch")]
    public class BatchController : ApiController
    {
        [HttpGet]
        [Route("getall")]
        public List<Batch> GetAllBatches()
        {
            using (BatchContext context = new BatchContext())
            {
                List<Batch> batches = context.batches.ToList();
                return batches;
            }
        }

        [HttpGet]
        [Route("get")]
        public Batch GetBatch(int id)
        {
            using (BatchContext context = new BatchContext())
            {
                Batch batch = context.batches.Find(id);
                return batch;
            }
        }

        [HttpPost]
        [Route("new")]
        public void CreateBatch(string name)
        {
            using (BatchContext context = new BatchContext())
            {
                Batch batch = new Batch() { Name = name };

                context.batches.Add(batch);

                context.SaveChanges();
            }
        }

        [HttpPut]
        [Route("update")]
        public void UpdateBatch(int id, string name)
        {
            using (BatchContext context = new BatchContext())
            {
                Batch batch = context.batches.Find(id);
                batch.Name = name;

                context.SaveChanges();
            }
        }

        [HttpDelete]
        [Route("delete")]
        public void DeleteBatch(int id)
        {
            using (BatchContext context = new BatchContext())
            {
                Batch batch = context.batches.Find(id);
                context.batches.Remove(batch);

                context.SaveChanges();
            }
        }
        
    }
}
