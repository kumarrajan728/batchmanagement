﻿namespace BatchManagement.API.Models
{
    public class Student
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int BatchId { get; set; }
    }
}