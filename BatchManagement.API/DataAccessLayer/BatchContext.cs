﻿using BatchManagement.API.Models;
using System.Data.Entity;

namespace BatchManagement.API.DataAccessLayer
{
    public class BatchContext: DbContext
    {
        public BatchContext(): base("name=dbconn")
        {
            Database.SetInitializer<BatchContext>(null);
        }

        public DbSet<Batch> batches { get; set; }
        public DbSet<Student> students { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Batch>().ToTable("Batch");
            modelBuilder.Entity<Student>().ToTable("Student");
        }
    }
}